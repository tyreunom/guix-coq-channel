Guix Coq Channel
================

This is a [Guix](https://guix.gnu.org) channel for coq users.  This channel
contains multiple versions of coq, so you can test and build your projects
with different versions.

My main use-case for this is to test the
[coquille](https://framagit.org/tyreunom/coquille) plugin for neovim on multiple
versions of coq.

What is coq?
------------

[Coq](https://coq.inria.fr) is a proof assistant for higher-order logic, which
allows the development of computer programs consistent with their formal
specification.

Installing
----------

This is a Guix channel. You will first need to [install](https://www.gnu.org/software/guix/download/)
Guix itself. Then, simply create a new `~/.config/guix/guix/channels.scm` file
with this content (or append the channel snippet if the file already exists):

```scheme
(cons* (channel
        (name 'coq)
        (url "https://framagit.org/tyreunom/guix-coq-channel.git"))
       %default-channels)
```

Then run `guix pull` to pull the new channel.

Channel Policy
--------------

* Only free software
* Only coq versions different from current Guix master
* Libraries and applications built with a coq version from this channel

This channel code is released under the GPL 3.0 or later license.  You will
find the complete text in the [LICENSE](LICENSE) file.

You are encouraged to contribute, report bugs and advertise this channel by any
means, but please read the [GNU Kind Communication Guidelines](https://www.gnu.org/philosophy/kind-communication.html)
and [Guix' Code of Conduct](https://git.savannah.gnu.org/cgit/guix.git/tree/CODE-OF-CONDUCT).
Although you are not *required* to follow these rules, I expect that you will
stay polite when communicating on this channel and elsewhere :)