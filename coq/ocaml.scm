;;; Guix Coq Channel --- Functional package management for GNU
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of Guix Coq Channel.
;;;
;;; The Guix Coq Channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Guix Coq Channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with the Guix Coq Channel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (coq ocaml)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system dune)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages ocaml)
  #:use-module (gnu packages pkg-config))

(define-public ocaml-cairo2
  (package
    (name "ocaml-cairo2")
    (version "0.6.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/Chris00/ocaml-cairo")
                     (commit version)))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0wzysis9fa850s68qh8vrvqc6svgllhwra3kzll2ibv0wmdqrich"))))
    (build-system dune-build-system)
    (arguments
     `(#:test-target "tests"))
    (inputs
     `(("cairo" ,cairo)
       ("gtk+-2" ,gtk+-2)
       ("lablgtk" ,lablgtk)))
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (home-page "https://github.com/Chris00/ocaml-cairo")
    (synopsis "Binding to Cairo, a 2D Vector Graphics Library")
    (description "Ocaml-cairo2 is a binding to Cairo, a 2D graphics library
with support for multiple output devices. Currently supported output targets
include the X Window System, Quartz, Win32, image buffers, PostScript, PDF,
and SVG file output.")
    (license license:lgpl3+)))

(define-public ocaml-lablgtk3
  (package
    (name "ocaml-lablgtk3")
    (version "3.0.beta8")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/garrigue/lablgtk")
                     (commit version)))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "08pgwnia240i2rw1rbgiahg673kwa7b6bvhsg3z4b47xr5sh9pvz"))))
    (build-system dune-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'make-writable
           (lambda _
             (for-each (lambda (file) (chmod file #o644)) (find-files "." "."))
             #t)))))
    (propagated-inputs
     `(("ocaml-cairo2" ,ocaml-cairo2)))
    (inputs
     `(("camlp5" ,camlp5)
       ("gtk+" ,gtk+)
       ("gtksourceview-3" ,gtksourceview-3)
       ("gtkspell3" ,gtkspell3)))
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (home-page "https://github.com/garrigue/lablgtk")
    (synopsis "OCaml interface to GTK+3")
    (description "LablGtk is an OCaml interface to GTK+ 1.2, 2.x and 3.x.  It
provides a strongly-typed object-oriented interface that is compatible with the
dynamic typing of GTK+.  Most widgets and methods are available.  LablGtk
also provides bindings to gdk-pixbuf, the GLArea widget (in combination with
LablGL), gnomecanvas, gnomeui, gtksourceview, gtkspell, libglade (and it can
generate OCaml code from .glade files), libpanel, librsvg and quartz.")
    ;; Version 2 only, with linking exception
    (license license:lgpl2.0)))
