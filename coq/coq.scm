;;; Guix Coq Channel --- Functional package management for GNU
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of Guix Coq Channel.
;;;
;;; The Guix Coq Channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Guix Coq Channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with the Guix Coq Channel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (coq coq)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system dune)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system ocaml)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (coq ocaml)
  #:use-module (gnu packages)
  #:use-module (gnu packages coq)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages ocaml)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python))

(define-public coq-8.6
  (package
    (name "coq")
    (version "8.6.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/coq/coq/archive/V"
                                  version ".tar.gz"))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "02nm5sn79hrb9fdmkhyclk80jydadf4jcafmr3idwr5h4z56qbms"))))
    (build-system ocaml-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'configure 'fix-latest-ocaml
           (lambda* (#:key inputs #:allow-other-keys)
             (substitute* "Makefile.build"
               (("MLINCLUDES=") (string-append
                                  "MLINCLUDES=-I "
                                  (assoc-ref inputs "ocaml-num")
                                  "/lib/ocaml/site-lib ")))
             (substitute* "configure.ml"
               (("CAMLFLAGS=") "CAMLFLAGS=-unsafe-string -package num "))
             (substitute* "ide/ideutils.ml"
               (("String.blit") "Bytes.blit"))
             (substitute* "tools/coqmktop.ml"
               (("nums") (string-append (assoc-ref inputs "ocaml-num")
                                        "/lib/ocaml/site-lib/nums"))
               (("\"-linkall\"") "\"-linkall\" :: \"-package\" :: \"num\""))
             #t))
         (replace 'configure
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (mandir (string-append out "/share/man"))
                    (browser "icecat -remote \"OpenURL(%s,new-tab)\""))
               (invoke "./configure"
                       "-prefix" out
                       "-mandir" mandir
                       "-browser" browser
                       "-coqide" "opt"))
             #t))
         (replace 'build
           (lambda* (#:key inputs #:allow-other-keys)
             (invoke "make" "-j" (number->string
                                  (parallel-job-count))
                     "world")
             #t))
         (delete 'check)
         (add-after 'install 'check
           (lambda _
             (with-directory-excursion "test-suite"
               (invoke "make"))
             #t)))))
    (inputs
     `(("lablgtk" ,lablgtk)
       ("python" ,python-2)
       ("camlp5" ,camlp5)
       ("ocaml-num" ,ocaml-num)))
    (native-search-paths
     (list (search-path-specification
            (variable "COQPATH")
            (files (list "lib/coq/user-contrib")))))
    (home-page "https://coq.inria.fr")
    (synopsis "Proof assistant for higher-order logic")
    (description
     "Coq is a proof assistant for higher-order logic, which allows the
development of computer programs consistent with their formal specification.
It is developed using Objective Caml and Camlp5.")
    ;; The code is distributed under lgpl2.1.
    ;; Some of the documentation is distributed under opl1.0+.
    (license (list license:lgpl2.1 license:opl1.0+))))

(define-public coq-8.7
  (package
    (inherit coq-8.6)
    (name "coq")
    (version "8.7.2")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/coq/coq/archive/V"
                                  version ".tar.gz"))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1lkqvs7ayzv5kkg26y837pg0d6r2b5hbjxl71ba93f39kybw69gg"))))
    (arguments
      (substitute-keyword-arguments (package-arguments coq-8.6)
        ((#:phases phases)
         `(modify-phases ,phases
            (delete 'fix-latest-ocaml)
            (replace 'check
              (lambda _
                (with-directory-excursion "test-suite"
                  ;; These two tests fail.
                  ;; This one fails because the output is not formatted as expected.
                  (delete-file-recursively "coq-makefile/timing")
                  ;; This one fails because we didn't build coqtop.byte.
                  (delete-file-recursively "coq-makefile/findlib-package")
                  (invoke "make"))
                #t))
            (replace 'build
              (lambda* (#:key inputs #:allow-other-keys)
                (substitute* "ide/ideutils.ml"
                  (("Bytes.unsafe_to_string read_string") "read_string"))
                (invoke "make" "-j" (number->string
                                     (parallel-job-count))
                        (string-append
                          "USERFLAGS=-I "
                          (assoc-ref inputs "ocaml-num")
                          "/lib/ocaml/site-lib")
                        "world")
                #t))
            ))))
    (native-inputs
     `(("ocaml-ounit" ,ocaml-ounit)))))

(define-public coq-8.8
  (package
    (inherit coq-8.7)
    (name "coq")
    (version "8.8.2")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/coq/coq/archive/V"
                                  version ".tar.gz"))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0i2hs0i6rp27cy8zd0mx7jscqw5cx2y0diw0pxgij66s3yr47y7r"))))
    (arguments
      (substitute-keyword-arguments (package-arguments coq-8.7)
        ((#:phases phases)
         `(modify-phases ,phases
            (replace 'build
              (lambda _
                (invoke "make"
                        "-j" (number->string (parallel-job-count))
                        "world")))))))))

(define-public coq-8.9
  (package
    (inherit coq-8.8)
    (name "coq")
    (version "8.9.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/coq/coq.git")
                    (commit (string-append "V" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1p4z967s18wkblayv12ygqsrqlyk5ax1pz40yf4kag8pva6gblhk"))))
    (arguments
      (substitute-keyword-arguments (package-arguments coq-8.7)
        ((#:phases phases)
         `(modify-phases ,phases
            (add-after 'unpack 'make-git-checkout-writable
              (lambda _
                (for-each make-file-writable (find-files "."))
                #t))))))))

(define-public coq-8.10+beta1
  (package
    (inherit coq-8.9)
    (name "coq")
    (version "8.10+beta1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/coq/coq")
                     (commit (string-append "V" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32 "02w6vgc4d3cl2jxv496048rxdh1gnq3jfr7hc11r7jw9rpg4ys2b"))))
    (inputs
     `(("lablgtk" ,ocaml-lablgtk3)
       ,@(package-inputs coq-8.9)))))

(define-public coq-8.10+beta2
  (package
    (inherit coq-8.10+beta1)
    (name "coq")
    (version "8.10+beta2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/coq/coq")
                     (commit (string-append "V" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32 "056m0bn4wqv3bajwz6rrnv8npd0nxyx7b3flkb1x78zjfzfcgscn"))))))

(define-public coq-8.10+beta3
  (package
    (inherit coq-8.10+beta2)
    (name "coq")
    (version "8.10+beta3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/coq/coq")
                     (commit (string-append "V" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32 "1rxdkk6rcvs5k639x4xr6a0sbq182r2g6h81cqk85117ayff86d2"))))))

(define-public coq-8.10
  (package
    (inherit coq-8.10+beta2)
    (name "coq")
    (version "8.10.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/coq/coq")
                     (commit (string-append "V" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32 "0553biaapi8i1ianx6mf88d62xa96477wz3qzrwmw6m3iwzaccl2"))))))

(define-public coq-8.10.1
  (package
    (inherit coq-8.10)
    (name "coq")
    (version "8.10.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/coq/coq")
                     (commit (string-append "V" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32 "10r8gswdkiz9yvhzx9bbs8f002p7hkwffg6vzh2ynsjlr10pmjp8"))))))
